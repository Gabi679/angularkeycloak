
import { KeycloakConfig } from 'keycloak-angular';

// Add here your keycloak setup infos
let keycloakConfig: KeycloakConfig = {
  url: 'http://localhost:10080/auth',
  realm: 'myRealm',
  clientId: 'angular-frontend',
  credentials: {
    secret: "ccfbc68a-6b08-43e1-88ee-6903bda10a56"
  }
};

export const environment = {
  production: false,
  keycloakConfig: keycloakConfig,
};