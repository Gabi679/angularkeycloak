import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AppAuthGuard } from './app-auth.guard';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';

const appRoutes: Routes = [
  { 
    path: 'user', 
    component: UserComponent,
    canActivate: [AppAuthGuard], 
    data: { roles: ['user'] }
  },
  { 
    path: 'admin', 
    component: AdminComponent,
    canActivate: [AppAuthGuard], 
    data: { roles: ['admin'] }
  },
 ];

 @NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}